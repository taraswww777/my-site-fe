export type MenuItem = {
  link: string,
  title: string
}
