import {Component, OnInit} from '@angular/core';
import {MenuItem} from '../menu/menu.component.types';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuItems: MenuItem[] = [
    {link: '#', title: 'HOME'},
    {link: '#', title: 'OUR WORK'},
    {link: '#', title: 'ABOUT US'},
    {link: '#', title: 'CONTACT'},
    {link: '#', title: 'SHORTCODES'},
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
