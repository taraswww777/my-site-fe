import {Component, Input, OnInit} from '@angular/core';
import {PortfolioType} from '../portfolio/portfolio.component.types';

@Component({
  selector: 'app-portfolio-nav',
  templateUrl: './portfolio-nav.component.html',
  styleUrls: ['./portfolio-nav.component.scss']
})
export class PortfolioNavComponent implements OnInit {
  @Input() types: PortfolioType[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
