import {Component, OnInit} from '@angular/core';
import {PortfolioItem, PortfolioType} from './portfolio.component.types';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  types: PortfolioType[] = [
    {title: 'Вёрстка', code: 'templating'},
    {title: 'Разработка', code: 'development'},
  ];

  list: PortfolioItem[] = [
    {title: 'Проект 1', code: 'p'},
    {title: 'Проект 2', code: 'p'},
    {title: 'Проект 3', code: 'p'},
    {title: 'Проект 11', code: 'p'},
    {title: 'Проект 12', code: 'p'},
    {title: 'Проект 13', code: 'p'},
    {title: 'Проект 13', code: 'p'},
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

}
