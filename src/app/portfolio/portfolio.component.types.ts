export type PortfolioType = {
  title: string,
  code: string,
};

export type PortfolioItem = {
  title: string,
  code: string,
};
