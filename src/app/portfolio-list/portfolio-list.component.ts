import {Component, OnInit, Input} from '@angular/core';
import {PortfolioItem} from '../portfolio/portfolio.component.types';

@Component({
  selector: 'app-portfolio-list',
  templateUrl: './portfolio-list.component.html',
  styleUrls: ['./portfolio-list.component.scss']
})
export class PortfolioListComponent implements OnInit {
  @Input() list: PortfolioItem[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
