import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BemModule} from 'angular-bem';

import {AppComponent} from './app.component';
import {MainPageComponent} from './main-page/main-page.component';
import {HeaderComponent} from './header/header.component';
import {MenuComponent} from './menu/menu.component';
import {PortfolioComponent} from './portfolio/portfolio.component';
import {PortfolioListComponent} from './portfolio-list/portfolio-list.component';
import {PortfolioNavComponent} from './portfolio-nav/portfolio-nav.component';
import {PortfolioItemComponent} from './portfolio-item/portfolio-item.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    HeaderComponent,
    MenuComponent,
    PortfolioComponent,
    PortfolioListComponent,
    PortfolioNavComponent,
    PortfolioItemComponent
  ],
  imports: [
    BrowserModule,
    BemModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
